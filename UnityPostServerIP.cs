public class tips
{
    public string JsonSendIP(string json, string ip)
    {
        var postData = Encoding.UTF8.GetBytes(json);

        var request = new UnityWebRequest(ip, UnityWebRequest.kHttpVerbPOST)
        {
            uploadHandler = new UploadHandlerRaw(postData),
            downloadHandler = new DownloadHandlerBuffer()
        };

        request.SetRequestHeader("Content-Type", "application/json");

        var operation = request.SendWebRequest();
        string result = "";
        operation.completed += _ =>
        {
            result = operation.webRequest.downloadHandler.text;
        };
        return result;
    }
}
