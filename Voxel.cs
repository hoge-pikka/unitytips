using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Voxel : MonoBehaviour
{
    [SerializeField] private GameObject PlaceBlock;

    // Update is called once per frame
    void Update()
    {
        SpawnBlock();
        DeleteBlock();
    }
    void SpawnBlock()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                float size = PlaceBlock.transform.localScale.x;
                Vector3 hitPointClamp = hit.point;
                if(hit.normal.x == 1)
                {
                    hitPointClamp.x = hit.collider.gameObject.transform.position.x + size;
                    hitPointClamp.y = hit.collider.gameObject.transform.position.y;
                    hitPointClamp.z = hit.collider.gameObject.transform.position.z;
                }
                else if (hit.normal.x == -1)
                {
                    hitPointClamp.x = hit.collider.gameObject.transform.position.x - size;
                    hitPointClamp.y = hit.collider.gameObject.transform.position.y;
                    hitPointClamp.z = hit.collider.gameObject.transform.position.z;
                }
                else if (hit.normal.y == 1)
                {
                    hitPointClamp.x = hit.collider.transform.position.x;
                    hitPointClamp.y = hit.collider.gameObject.transform.position.y+ size;
                    hitPointClamp.z = hit.collider.transform.position.z;
                }
                else if (hit.normal.y == -1)
                {
                    hitPointClamp.x = hit.collider.transform.position.x;
                    hitPointClamp.y = hit.collider.gameObject.transform.position.y - size;
                    hitPointClamp.z = hit.collider.transform.position.z;
                }
                else if (hit.normal.z == 1)
                {
                    hitPointClamp.x = hit.collider.transform.position.x;
                    hitPointClamp.y = hit.collider.gameObject.transform.position.y;
                    hitPointClamp.z = hit.collider.transform.position.z+ size;
                }
                else if (hit.normal.z == -1)
                {
                    hitPointClamp.x = hit.collider.transform.position.x;
                    hitPointClamp.y = hit.collider.gameObject.transform.position.y;
                    hitPointClamp.z = hit.collider.transform.position.z - size;
                }
                Instantiate(PlaceBlock, hitPointClamp, Quaternion.identity);
            }
        }
    }
    void DeleteBlock()
    {
        if (Input.GetMouseButtonDown(1))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 60f))
            {
                Destroy(hit.transform.gameObject);
            }
        }
    }
}
